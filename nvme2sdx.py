#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import apt
import argparse
import glob
import json
import platform
import subprocess
import sys

runningOs = platform.linux_distribution()
if runningOs[0]!= "Ubuntu":
    print("Sorry. The script is running only on Ubuntu")
    sys.exit(1)

parser = argparse.ArgumentParser(description='Convert NVME*n1 to sdX')
parser.add_argument('--install', help='Try to install nvme-cli, if not exists in system',  action='store_true', dest='install_flag')
parser.add_argument('--csv', help='Return csv instead of json',  action='store_true', dest='csv_flag')
args = parser.parse_args()

cache = apt.cache.Cache()
cache.update()
cache.open()
pkg = cache['nvme-cli']
if pkg.is_installed:
    diskDict = {}
    files = glob.glob("/dev/nvme*n1")
    if len(files) == 0:
        print("No nvme volumes")
    for item in files:
        output = subprocess.check_output(["nvme", "id-ctrl", "-v", item])
        outputLines = output.decode('utf-8').split("\n")
        
        for line in outputLines:
            if line.startswith("0000:"):
                diskDict[item] = line.split('"')[1].split('.')[0]
    
    if args.csv_flag == True:
        for i in diskDict.keys():
            print(i+","+diskDict[i])
    else:
        print(json.dumps(diskDict))

elif args.install_flag == True:
    pkg.mark_install()
    try:
        cache.commit()
    except Exception:
        print("Sorry, package installation failed")
    
else:
    print("Missing 'nvme-cli' please install it manually, or run the script with --install option")
    sys.exit(1)
